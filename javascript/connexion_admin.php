<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">

	<?php
		include("header.php");

	?>

	<div class="container-fluid ">
		
		<div class="row ">
			<h2></h2>
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10">
				<h3>CONNECTEZ VOUS 	ADMIN</h3>
				<form enctype="multipart/form-data" method="post" action="traitement_connexion_admin.php" id="myform" >
					<div class="row">
						<div class=" col-md-offset-3 col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
							<div class="form-group">	
								<span class="glyphicon glyphicon-envelope"></span>
								<label>email</label>
								<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
								 <p><?php if (isset($_SESSION['message1'])) {
	                  			echo $_SESSION['message1'];
	                  			}  ?></p><br>
							</div>
							
							<div class="form-group">	
								<span class="glyphicon glyphicon-lock"></span>
								<label>mot de passe</label>
								<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								<p><?php if (isset($_SESSION['message2'])) {
	                  			echo $_SESSION['message2'];
	                  			}  ?></p><br>	
							</div>
						</div>
					</div>
					<div class="row mout">	
						<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6">
							<input  type="reset"  class="  btn btn-block btn-info bout1"> 
						</div>
						<div class="  col-md-4 col-xm-5 col-xs-6">
							<input  type="submit"  class="  btn btn-block btn-info btn-success" >
						</div>
					</div>
					<!-- <div class="col-md-3">
						<input  type="submit" name="enregistrer" class=" bout btn btn-block btn-info" >
					</div> -->
						
				</form>		
			</div>
		</div>
		
	</div>
	<?php session_destroy(); ?> 
	</body>
</html>	