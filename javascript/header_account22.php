<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">
	<div class="container">
		<div class="row">
			<nav class="navbar navbar-expand-lg ">
		  		<div class=" navbar-collapse pull pull-right" >
			    
			      	<ul class="navbar-nav">
				      	<li><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
				      	<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
				        	</a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					        	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="#">Mon profil</a>
					          	<div class="dropdown-divider"></div>
					          	<form method="post" action="deconnexion.php">
					          		<a class="dropdown-item" href="#">Deconnexion</a>
					          	</form>
					          	<div class="dropdown-divider"></div>
					        </div>
				      	</li>
				    </ul>
			      	
			    
			 	 </div>
			</nav>
		</div>
	</div>


	<script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>