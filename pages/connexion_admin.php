<?php session_start(); ?>

	<?php
		include("header.php");
	?>
	<?php if (isset($_SESSION['USER']) AND $_SESSION['USER']['niveau']==5) {
        header('location:header_account_administrateur.php');
    }else{ ?>

		<div class="container-fluid corps">
			<div class="row ">
				<div class=" form1 col-md-offset-2 col-md-8  col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10">
					<h3>CONNECTEZ VOUS 	ADMIN</h3>
					<form enctype="multipart/form-data" method="post" action="traitement_connexion_admin.php" id="myform" >
						<div class="row">
							<div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10 form2">
								<div class="form-group">	
									<span class="glyphicon glyphicon-envelope"></span>
									<label>email</label>
									<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
									 <br>
								</div>
								
								<div class="form-group">	
									<span class="glyphicon glyphicon-lock"></span>
									<label>mot de passe</label>
									<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
									<br>
									<p><?php if (isset($_SESSION['message'])) {
		                  			echo $_SESSION['message'];
		                  			}  ?></p>	
								</div>
							</div>
						</div>
						<div class="row form2 form4">	
							<div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10 col-xs-offset-2 col-xs-offset-8">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-5">
										<input  type="reset"  class="  btn btn-block btn-info"> 
									</div>
									<div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-1 col-xs-5">
										<input  type="submit"  class="  btn btn-block btn-info" >
									</div>
								</div>
							</div>
						</div>
					</form>		
				</div>
			</div>
		</div>
	</body>
</html>	
<?php } ?>



