<?php session_start();
?>


<!DOCTYPE html>
<html>
    <head>
    	<title><?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></title>
    	<meta charset="utf-8">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    	<link rel="stylesheet" type="text/css" href="../css/style.css">
    	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
    	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
    </head>
    <body>
    	
    		<section class="corps">
    				<div class="navbar navbar-expand">
    					<div class="col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-4 col-xs-offset-3 col-xs-4">
    						<span class="navbar-brand"><h1>Inchclass</h1></span>
    					</div>
    					<div class="col-md-offset-8 col-sm-offset-7 connet">
    						<ul class="nav navbar-nav">
    							<li class="nom"><span ><?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
    							<li class="dropdown"><a data-toggle="dropdown" href="#">
    								<?php echo "<img class='imge' src='../images/".$_SESSION["USER"]["photo"]. "' >" ?> <span class="caret"></span></a>
    							<ul class="dropdown-menu">
    								<li><a class="dropdown-item" href="profil_administrateur.php">Mon profils</a></li>
    								<li><a class="dropdown-item" href="deconnexion.php">Deconection</a></li>
    							</ul>
    							</li>	 
    						</ul>
    					</div>
    				</div>
    		</section>
            <section>
                <div class="container">
                <div class="row text-center" style="color: aqua; font-size: 30px; margin: 25px 0; "> Liste Des Membres Inscrits Sur La Plateforme </div>
                <table class="table table-bordered table-striped table-sm table-responsive">
                    <thead>
                        <tr>
                            <th class="text-center n°"> N° </th>
                            <th class="text-center id"> id </th>
                            <th class="text-center profil"> Profil </th>
                            <th class="text-center nom"> Nom </th>
                            <th class="text-center prenom"> Prenom </th>
                            <th class="text-center etat"> Etat </th>
                            <th class="text-center action"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $bdd = new PDO('mysql:host=localhost;dbname=users', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                        $response = $bdd->query('SELECT * FROM utilisateur WHERE niveau!=5');
                        $i = 0;
                        $user = array();
                        while ($donnees = $response->fetch()) {
                            $user[$i] = $donnees; ?>
                                <tr>
                                    <td ><?php echo $i; ?></td>
                                    <td class="id" ><?php echo $donnees['id']; ?></td>
                                    <td class="profil" ><?php echo '<a href="../images/'.$donnees['photo'].'"><img src="../images/'.$donnees['photo']. '"" class="img-rounded text-center imge" alt="photo_profil" title="cliquez ici pour agrandir" /></a>' ?></td>
                                    <td ><?php echo $donnees['nom']; ?></td> 
                                    <td ><?php echo $donnees['prenom']; ?></td> 
                                    <td>
                                        <?php if ($donnees['niveau'] == 1) { ?>
                                            <span style='color:green;font-weight:800;'> actif <span>
                                        <?php } else if ($donnees['niveau'] == 2) {  ?>
                                            <span style='color:orange;font-weight:800;'> inactif <span>
                                        <?php } else if ($donnees['niveau'] == 3) { ?>
                                            <span style='color:red;font-weight:800;'> deleted <span>
                                        <?php  } ?>
                                    </td>
                                    <td> 
                                        <div style="font-size: 1px;"> 
                                            <form enctype="multipart/form-data" method="post" action="Management555.php" style=" display: inline-block;">
                                                <a href="#">
                                                    <input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
                                                    <input type="hidden" name="N2"  value="consulter">
                                                    <button  type="submit" style="background-color:inherit; border: none;" title="consulter">
                                                        <span class="fa fa-eye editer" style="margin-left: 10px; color:aqua; font-size: 13px;"></span>
                                                    </button>
                                                </a>
                                            </form>
                                           <!--  <form enctype="multipart/form-data" method="post" action="Management555.php" style=" display: inline-block;">
                                                <a href="#">
                                                    <input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
                                                    <input type="hidden" name="N2"  value="editer">
                                                    <button  type="submit" style="background-color:inherit; border: none;" title="editer">
                                                        <span class="fa fa-pencil editer" style=" margin-left: 10px; color:blue; font-size: 13px;" alt="editer"></span>
                                                    </button>
                                                </a>
                                            </form> -->
                                            <?php if($donnees['niveau'] <=2 ){ ?>
                                            <form enctype="multipart/form-data" method="post" action="Management555.php" style=" display: inline-block;">
                                                <a href="#">
                                                    <input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
                                                    <input type="hidden" name="N2"  value="supprimer">
                                                    <button  type="submit" style="background-color:inherit; border: none;" title="supprimer">
                                                        <span class="fa fa-trash Supprimer" style="margin-left: 10px;color:red; font-size: 13px;"></span>
                                                    </button>
                                                </a>
                                            </form>
                                            <?php   } 
                                            if ($donnees['niveau'] == 1) { ?>
                                        <form enctype="multipart/form-data" method="post" action="Management555.php" style=" display: inline-block;">
                                            <a href="#">
                                                <input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
                                                <input type="hidden" name="N2"  value="desactiver">
                                                <button  type="submit" style="background-color:inherit; border: none;" title="desactiver">
                                                    <span class="glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 13px;"></span>
                                                </button>
                                            </a>
                                        </form>
                                            <?php   } else { ?>
                                           <form enctype="multipart/form-data" method="post" action="Management555.php" style=" display: inline-block;">
                                                <a href="#">
                                                    <input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
                                                    <input type="hidden" name="N2"  value="activer">
                                                    <button  type="submit" style="background-color:inherit; border: none;" title="activer">
                                                        <span class="glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 13px;"></span>
                                                    </button>
                                                </a>
                                           </form>
                                      </div>
                                    </td>
                                </tr>
                        <?php   }
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            </section>
    
    	<!-- <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script> -->
    	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
    	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
    	
    	<script type="text/javascript">

      </script>
    </body>
</html>


					