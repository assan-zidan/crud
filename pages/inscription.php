 <?php session_start(); ?>


	<?php
		include("header.php");
	?>

	<?php if (isset($_SESSION['USER']) AND $_SESSION['USER']['niveau']==1) {
		header('location:header_account_utilisateur.php');
	}else{ ?>
	<div class="container-fluid corps">
		<div class="row ">
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10">
				<h3>INSCRIVEZ VOUS</h3>
				<form enctype="multipart/form-data" method="post" action="traitement_inscription.php" id="myform" >
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-offset-1 col-xs-10 form2">
							<span class="glyphicon glyphicon-user"></span>
							<label>Noms</label>
							<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="veuillez entrer votre nom"  required="">
							<p></p>
							<span class="glyphicon glyphicon-user"></span>
							<label>Prenoms</label>
							<input class="form-control inpt2" type="text" name="prenom" placeholder="veuillez entrer votre nom"  id="prenom" required="">
							<p></p>
							<span class="glyphicon glyphicon-envelope"></span>
							<label>email</label>
							<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
							<p><?php if (isset($_SESSION['message_error1'])) {
	                  			echo $_SESSION['message_error1'];
	                  			}  ?></p>
							<span class="glyphicon glyphicon-lock"></span>
							<label>mot de passe</label>
							<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>
							<input type="hidden" name="niveau" value="1">
						</div>
						<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-sm-offset-1 col-sm-5 col-sm-pull-1  col-xs-offset-1 col-xs-10 form2 form7">
							<span class="glyphicon glyphicon-picture"></span>
							<label>Telecharger votre photo de pofil</label>
							<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
							<div class="col-md-offset-4 col-md-4 im" style="">
	                        	<div class="row"><img id="im"/></div>
	                        	<div class="row">
		                        	<p><?php if (isset($_SESSION['extension'])) {
		                        		echo $_SESSION['extension'];
		                        	}
		                        	elseif (isset($_SESSION['volume'])) {
		                        	 	echo $_SESSION['volume'];
		                        	 } 
		                        	elseif(isset($_SESSION['message_error'])) {
		                  			echo $_SESSION['message_error'];
		                  			// echo $_SESSION['volume'];
		                  			}
		                  			else{
		                  				$statut_img="ok";
		                  			}  ?></p>
		                  		</div>
	                  		</div>
						</div>
					</div>
					<div class="row form4 form2">
						<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6">
							<input  type="reset"  class="  btn btn-block btn-info bout1"> 
						</div>
						<div class="  col-md-4 col-xm-5 col-xs-6">
							<input  type="submit"  class="  btn btn-block btn-info btn-success" >
						</div>
					</div>
				</form>		
			</div>
		</div>	
	</div>
    
	<?php 
	  }
	  unset($_SESSION['extension']);
	  unset($_SESSION['message_error']);
	  unset($_SESSION['volume']);
	  unset($_SESSION['message_error1']);

	 ?>
	</body>
	<!-- <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script> -->
	<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('im');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
    var	nom = document.getElementById('name');
    var prenom=document.getElementById('prenom');
    var pwd=document.getElementById('pwd');
      
   


	nom.addEventListener('change', testnom); 
	function testnom(){
	 	if(nom.value.length<4){ 
	 		nom.setCustomValidity('Veuillez entrer un nom comprenant au moins 4 caracteres');
			return false;
		}
		else if(contient_specialchart(nom.value)){ 
			nom.setCustomValidity('Le nom doit avoir au minimum 4 lettres sans les caracteres speciaux tels que @ ou % ...');
			return false;
		}else{
			return true;
		}
	}


	prenom.addEventListener('change', testprenom); 
	function testprenom(){		
	 	if(prenom.value.length<3){ 
	 		prenom.setCustomValidity('Veuillez entrer un prenom comprenant au moins 4 caracteres');	  
			return false;
		}
		else if(contient_specialchart(prenom.value)){  
			prenom.setCustomValidity('Le prenom doit avoir au minimum 4 lettres sans les caracteres speciaux tels que @ ou % ...');
			return false;
		}else{
			return true;
		}
	}
	function contient_specialchart(char){
		var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';'];

		for (var i = special.length - 1; i >= 0; i--) { 
			for (var id in char) {  
				if(char[id]==special[i]){
					return true;
				}
			}
		}
		return false;
	}

	pwd.addEventListener('change', testpwd);
	function testpwd(){
		if(pwd.value.length<8){
			// pwd.setCustomValidity('Veuillez entrer un mot de passe comprenant au moins 8 lettres et caracteres speciaux');
			return false;
		}else{
			return true;
		}

	}

	


  </script>
  


</html>