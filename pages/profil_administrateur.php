  <?php session_start(); ?> 

<!DOCTYPE html>
<html>
<head>
	<title>profil <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body>
	<section class="corps">
			<div class="navbar navbar-expand ">
				<div class="col-md-offset-1 col-md-2 col-sm-3 col-xs-8">
					<span class="navbar-brand"><h1>Inchclass</h1></span>
				</div>
				<div class="col-md-offset-8 connet">
					<ul class="nav navbar-nav">
						<li class="nom"><span ><?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
						<li class="dropdown"><a data-toggle="dropdown" href="#">
							<?php echo "<img class='imge' src='../images/".$_SESSION["USER"]["photo"]. "' >" ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!-- <li><a class="dropdown-item" href="profils.php">Mon profils</a></li> -->
							<li><a class="dropdown-item" href="deconnexion.php">Deconection</a></li>
						</ul>
						</li>
					</ul>
				</div>
			</div>
	<!-- </section>
	<section> -->
		<div class="container-fluid corps ">
			<div class="row ">
				<div class=" form5 col-md-offset-3 col-md-6 col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8">
					<h3>profils d'utilisateur </h3>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-5">
							<div class="row modif">
								<span >Photo de profil:</span> 
							</div>
						</div>
						<div class=" col-md-5 col-sm-5 col-xs-5">
							<div class="row">
								<p><?php if (isset($_SESSION['USER'])) {
								echo "<img class=' img-thumbnail ' src='../images/".$_SESSION["USER"]["photo"]. "' >";
								} ?></p>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-5">
							<div class="row modif">
								<span >Nom:</span> 
							</div>
						</div>
						<div class=" col-md-5 col-sm-5 col-xs-5">
							<div class="row">
								<?php if (isset($_SESSION['USER'])) {
								echo $_SESSION['USER']['nom'];
								} ?>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-5">
							<div class="row modif">
								<span >Prenom:</span> 
							</div>
						</div>
						<div class=" col-md-5 col-sm-5 col-xs-5">
							<div class="row">
								<?php if (isset($_SESSION['USER'])) {
								echo $_SESSION['USER']['prenom'];
								} ?>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-5">
							<div class="row modif">
								<span >Email:</span> 
							</div>
						</div>
						<div class=" col-md-5 col-sm-5 col-xs-5">
							<div class="row">
								<?php if (isset($_SESSION['USER'])) {
								echo $_SESSION['USER']['email'];
								} ?>
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-offset-5 form2 form4 mform">	
							<button class="btn btn-info" data-toggle="modal" href="#infos"><span class="mdd" >modifier le profil</span></button>
						</div>
					</div>
				</div>	
			</div>
	</section>
	<div class="row modal modal-success" id="infos">
			<div class="col-md-offset-3 col-md-6">
				<div class="modal-content">
						<div class="modal-header">
							<button class="btn btn-info-close pull pull-right" data-dismiss="modal"><span class="mdd">X</span></button>
							<div class="modal-title">
								<h3>modification du profil</h3>
							</div>
						</div>
						<div class="modal-body">
							<form enctype="multipart/form-data" method="post" action="1111.php" id="myform" >
								<div class="row">
								<div class="col-md-5 col-sm-5 col-xs-offset-1 col-xs-10 form2">
								<span class="glyphicon glyphicon-USER"></span>
								<label>Nom</label>
								<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="veuillez entrer votre nom"  required="" value="<?php if(isset($_SESSION['USER'])){
									echo$_SESSION['USER']['nom'];
								} ?>">
								<p></p>
								<span class="glyphicon glyphicon-USER"></span>
								<label>Prenoms</label>
								<input class="form-control inpt2" type="text" name="prenom" placeholder="veuillez entrer votre nom"  id="prenom" required="" value="<?php if(isset($_SESSION['USER'])){
									echo$_SESSION['USER']['prenom'];
								} ?>">
								<p></p>
								<span class="glyphicon glyphicon-envelope"></span>
								<label>email</label>
								<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="" value="<?php if(isset($_SESSION['USER'])){
									echo$_SESSION['USER']['email'];
								} ?>"><br>
								<span class="glyphicon glyphicon-lock"></span>
								<label>Mot de passe</label>
								<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>
								<!-- <span class="glyphicon glyphicon-lock"></span>
								<label>Nouveau mot de passe</label>
								<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre nouveau mot de passe" id="pwd"  required=""><br> -->
								<input type="hidden" name="niveau" value="1">
								</div>
								<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-sm-offset-1 col-sm-5 col-sm-pull-1  col-xs-offset-1 col-xs-10 form2 form7">
									
								<span class="glyphicon glyphicon-picture"></span>
								<label>Telecharger votre photo nouvel de pofil</label>
								<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" >
								<div class="col-md-offset-4 col-md-4 im" style="">
		                        	<div class="row im"><img id="im" <?php if(isset($_SESSION['USER'])){
										echo"<img id='im' src='../images/".$_SESSION["USER"]["photo"]. "' >";
										} ?>
		                        	</div>
		                        	
		                  		</div>

								</div>
								</div>
								<!-- <div class="  col-md-4 col-xm-5 col-xs-6"> -->
									<!-- <input class="btn btn-info" type="submit" name="" value="Enregistrer"> -->
								<!-- </div> -->
								<!-- <div class="col-md-3">
									<input  type="submit" name="enregistrer" class=" bout btn btn-block btn-info" >
								</div> -->

								<div class="modal-footer">
								<!-- <button class=" btn btn-success" data-dismiss="modal"> sauvegarder </button> -->
								<input class="btn btn-info" type="submit" name="" value="Enregistrer">
								</div>
								
							</form>	
							
						</div>
						<!-- <div class="modal-footer">
							<button class=" btn btn-success" data-dismiss="modal"> sauvegarder </button>
							<input class="btn btn-info" type="submit" name="" value="Enregistrer">
						</div> -->
				</div>
			</div>
		</div>



	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">

  	</script>
</body>
</html>