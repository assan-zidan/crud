<?php 
   session_start();

 ?>

 
<?php
   
   if (isset($_POST) AND isset($_POST['email'])) {
      $bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
      $response=$bdd->query('SELECT * FROM utilisateur');
      $i=0;
      $allUser=array();
      while ($donnees =$response->fetch()) {
         $allUser[$i]=$donnees;
         if ($allUser[$i]['email']==$_POST['email']) {
            $_SESSION['message_error1']="Email existant!";
            header('location: inscription.php');
         }
         $i++;
      }
      
      if (isset($_FILES['photo']) AND $_FILES['photo']['error'] == 0)
      {
         if ($_FILES['photo']['size'] <= 3000000)
         {
            $infosfichier = pathinfo($_FILES['photo']['name']);
            $extension_upload = $infosfichier['extension'];
            $extensions_autorisees = array( 'png','PNG','jpeg','jpg','JPG','JPEG');
            if (in_array($extension_upload, $extensions_autorisees))
            {
               move_uploaded_file($_FILES['photo']['tmp_name'], '../images/' . basename($_FILES['photo']['name']));
               $statut_img="ok";
            }else{
               echo 'extention non-autorisee';
               $_SESSION['extension']="extension non autoriser";
               $statut_img="non";
            }
         }else{
            echo 'Image trop Volumineuse';
            $_SESSION['volume']="Image trop Volumineuse";
            $statut_img="non";
         }
      }

      if ($statut_img=="ok") {
         $dataInsert=$bdd->prepare('INSERT into utilisateur (nom,prenom,email,pwd,photo,niveau) VALUES(?,?,?,?,?,?)');
         $dataInsert->execute( array($_POST['nom'],$_POST['prenom'],$_POST['email'],$_POST['pwd'],$_FILES['photo']['name'],$_POST['niveau']));
         $allUser=array();
         $j=0;
         $response=$bdd->query('SELECT * FROM utilisateur');
         while ($donnees =$response->fetch()) {
            $allUser[$j]=$donnees;
            $j++;
         }
         for ($i=0; $i <$j ; $i++) { 
            if ($allUser[$i]['email']==$_POST['email']) {
               $_SESSION['USER']=$allUser[$i];
                header('location: header_account_utilisateur.php');
            }
         }
         
      }else{
         $_SESSION['message_error']="Image  incorrecte!";
         header('location: inscription.php');
      }
           
   }else{
      header('location: inscription.php');
  }
  
?>
</tbody>
</table>
  </div>


</body>
</html>



